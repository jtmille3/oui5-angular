// Scan openui5 for Controls and Components
// Read the metadata
// Generate angular directives based on the metadata

const walk = require('walk');
const fs = require('fs');
const esprima = require('esprima');
const files = [];

const walker  = walk.walk('./openui5/resources/sap', { followLinks: false });

// walker.on('file', function(root, stat, next) {
//     // Add this file to the list of files
//     if (stat.name.endsWith('-dbg.js')) {
//       files.push(root + '/' + stat.name);
//     }
//     next();
// });
//
// walker.on('end', function() {
//     console.log(files);
//     for (var i = 0; i < files.length; i++) {
//       readFile(files[i]);
//     }
// });

readFile('./openui5/resources/sap/m/Button-dbg.js');

var count = 0;
var failure = 0;
function readFile(file) {
  fs.readFile(file, 'utf8', function (error, data) {
      count++;
      if (error) throw error;

      var output = data.toString();
      console.log(file);
      console.log(JSON.stringify(esprima.tokenize(output)));
      // console.log(JSON.stringify(esprima.parse(output)));
      // console.log(esprima.parseScript(output, { tokens: true }));
  });
}
