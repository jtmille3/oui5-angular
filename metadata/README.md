# README for generating html commons - oui5 metadata

openui-framework is the project that contains all the honeycomb components that
all SAS groups consume.  To build the project you must check it out because
there is at least one gradle task that looks for something under .git.  Because
of this I cloned the project while I wait for access to the code base.

(jemill/openui-framework)[https://gitlab.sas.com/jemill/openui-framework]

  git clone https://gitlab.sas.com/jemill/openui-framework
  ./gradlew build

At this point the project needs to be patched to generate the XML and JSON
metadata.  

There is probably an easier way to pass in the publishing variants, but for now
I'm modifying the code to force it.  (TODO: look into passing the variant through
a -P parameter from gradle.  e.g. -Ppublish=apijson)

* Copy monkey-patched guides.backup to openui-framework/node_modules/guides-buildtools-openuibundled/node_modules/guides-buildtools-openui/node_modules/guides-buildtools-jsdoc/lib/guides-buildtools-jsdoc-lib.js

There is a bug in the publish.js that prevents it from generating when a parameter name is missing.

* Copy monkey-patched publish.backup to openui-framework/node_modules/guides-buildtools-openuibundled/node_modules/guides-buildtools-openui/node_modules/guides-buildtools-jsdoc/templates/sapui5-revised/publish.js

Now run the doc task from gradle.

  ./gradlew generateJSDocWithDependencies --rerun-tasks

The output gets placed into target/jsdoc subdirectory.
