module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-ts');
    grunt.loadNpmTasks('grunt-prettier');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.initConfig({
        ts: {
          default : {
            tsconfig: './tsconfig.json'
          }
        },
        prettier: {
          options: {
            singleQuote: true,
            useTabs: true,
            parser: 'typescript'
          },
          files: {
            src: [
                'src/**/*.ts'
            ]
          }
        },
        uglify: {
            js: { //target
                src: ['src/angular-oui5-bridge.js'],
                dest: 'src/angular-oui5-bridge-min.js'
            }
        }
    });

    grunt.task.registerTask('default', ['prettier', 'ts']);

    grunt.registerTask('build', ['prettier', 'ts', 'uglify']);
};
