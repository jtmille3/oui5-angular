// const fs = require('fs');
const fs = require('fs-extra');

const outputDirectory = './src';

if (!fs.existsSync(outputDirectory)) {
  fs.mkdirpSync(outputDirectory);
} else {
  fs.removeSync(outputDirectory);
  fs.mkdirpSync(outputDirectory);
}

fs.readFile('./metadata/api.json', 'utf8', function (error, data) {
    if (error) throw error;

    const output = data.toString();
    const json = JSON.parse(output);

    const symbols = json['symbols'];
    const filesMap = {};

    for(let i = 0; i < symbols.length; i++) {
      const symbol = symbols[i];
      const name = symbol.name;
      const kind = symbol.kind;

      if (symbol &&
          symbol['ui5-metadata'] &&
          symbol['ui5-metadata']['stereotype']) {
          filesMap[name] = symbol;
      }
    }

    const files = Object.keys(filesMap);
    files.forEach(function(file) {
      const symbol = filesMap[file];

      const path = outputDirectory + '/' + getFolderPackage(symbol.name);
      fs.mkdirpSync(path);

      generateAngularComponent(symbol, filesMap);
    });

    generateOui5Control();
    generateOui5Component();
    generateStubs();
    generateDefineJS(filesMap);
});

function isUI5(symbol) {
  return symbol &&
      symbol['ui5-metadata'] &&
      symbol['ui5-metadata']['stereotype'];
}

function isOUI5Control(symbol) {
  // return isUI5(symbol) &&
  //     symbol['ui5-metadata']['stereotype'] === 'control';
  return isUI5(symbol);
}

function getFolderPackage(name) {
  const lastIndex = name.lastIndexOf('.');
  const packagePath = name.substring(0, lastIndex);
  return packagePath.replace(/\./g, '/');
}

function getFileName(name) {
  const lastIndex = name.lastIndexOf('.');
  return name.substring(lastIndex + 1, name.length);
}

function generateDefineJS(filesMap) {
  const sapTemplate = getDefineJSTemplate('sap-define', filesMap, 'sap');
  fs.writeFileSync(`${outputDirectory}/sap-define.ts`, sapTemplate, function(err) {
    if (err) throw error;
  });

  const sasTemplate = getDefineJSTemplate('sas-define', filesMap, 'sas');
  fs.writeFileSync(`${outputDirectory}/sas-define.ts`, sasTemplate, function(err) {
    if (err) throw error;
  });
}

function generateStubs() {
  const stubsTemplate = getStubsTemplate();
  fs.writeFileSync(`${outputDirectory}/stubs.ts`, stubsTemplate, function(err) {
    if (err) throw error;
  });
}

function generateAngularComponent(symbol, filesMap) {
  const template = getComponentTemplate(symbol, filesMap);
  fs.writeFileSync(`${outputDirectory}/${getFolderPackage(symbol.name)}/${getFileName(symbol.name)}.component.ts`, template, function(err) {
    if(err) throw error;
  });
}

function generateOui5Component() {
  const oui5ComponentTemplate = getOui5ComponentTemplate();
  fs.writeFileSync(`${outputDirectory}/oui5Control.component.ts`, oui5ComponentTemplate, function(err) {
    if (err) throw error;
  });
}

function generateOui5Control() {
  const oui5ControlTemplate = getOui5ControlTemplate();
  fs.writeFileSync(`${outputDirectory}/oui5Control.ts`, oui5ControlTemplate, function(err) {
    if (err) throw error;
  });
}

function getControllerName(name) {
  const parts = name.split('.');
  let string = '';
  for (var i = 0; i < parts.length; i++) {
    const part = parts[i];
    string += part.substring(0, 1).toUpperCase() + part.substring(1, part.length);
  }
  return string;
}

function getComponentName(name) {
  const componentName = getControllerName(name);
  return componentName.substring(0, 1).toLowerCase() + componentName.substring(1);
}

function getInstantiateObject(name) {
  const firstPackageNameIndex = name.indexOf('.');
  const firstPackageName = name.substring(0, firstPackageNameIndex);
  const lastPackageName = name.substring(firstPackageNameIndex + 1, name.length);
  return `(<any>${firstPackageName}).${lastPackageName}`;
}

function getPropertyCamelCase(name) {
  return name.substring(0, 1).toUpperCase() + name.substring(1);
}

function getProperties(symbol, filesMap) {
  const metadata = symbol['ui5-metadata'];

  const properties = metadata.properties;
  const propertiesMap = {};

  if (properties && properties.length) {
    properties.forEach(function(property) {
      propertiesMap[property.name] = property;
    });
  }

  if (!!symbol.extends) {
    map = getProperties(filesMap[symbol.extends], filesMap);

    Object.keys(map).forEach(function(key) {
      const value = map[key];
      propertiesMap[key] = value;
    });
  }

  return propertiesMap;
}

function getEvents(symbol, filesMap) {
  const metadata = symbol['ui5-metadata'];

  const events = metadata.events;
  const eventsMap = {};

  if (events && events.length) {
    events.forEach(function(event) {
      eventsMap[event.name] = event;
    });
  }

  if (!!symbol.extends) {
    map = getEvents(filesMap[symbol.extends], filesMap);

    Object.keys(map).forEach(function(key) {
      const value = map[key];
      eventsMap[key] = value;
    });
  }

  return eventsMap;
}

function getAngularBindings(symbol, filesMap) {
  const metadata = symbol['ui5-metadata'];

  let bindings = ``;

  const propertiesMap = getProperties(symbol, filesMap);
  Object.keys(propertiesMap).forEach(function(key) {
    const property = propertiesMap[key];
    bindings += `oui5${getPropertyCamelCase(property.name)}: '=?',\n`;
  });

  const eventsMap = getEvents(symbol, filesMap);
  Object.keys(eventsMap).forEach(function(key) {
    const event = eventsMap[key];
    bindings += `oui5${getPropertyCamelCase(event.name)}: '&',\n`;
  });

  return bindings;
}

function getOUI5ModelValues(symbol, filesMap) {
  let modelValues = ``;

  const propertiesMap = getProperties(symbol, filesMap);
  Object.keys(propertiesMap).forEach(function(key) {
    const property = propertiesMap[key];
    modelValues += `oui5${getPropertyCamelCase(property.name)}: this.oui5${getPropertyCamelCase(property.name)},\n`;
  });

  return modelValues;
}

function getOUI5ComponentValues(symbol, filesMap) {
  let componentValues = ``;

  const propertiesMap = getProperties(symbol, filesMap);
  Object.keys(propertiesMap).forEach(function(key) {
    const property = propertiesMap[key];
    componentValues += `${property.name}: '{/oui5${getPropertyCamelCase(property.name)}}',\n`;
  });

  return componentValues;
}

function getAngularPropertyWatches(symbol) {
  let watches = ``;

  const metadata = symbol['ui5-metadata'];
  const properties = metadata.properties;

  if (properties && properties.length) {
    properties.forEach(function(property) {
      watches += `
      this.$scope.$watch('$ctrl.oui5${getPropertyCamelCase(property.name)}', (newValue, oldValue) => {
        if (this.component.set${getPropertyCamelCase(property.name)}) {
          this.component.set${getPropertyCamelCase(property.name)}(newValue);
        }
      }, true);
      `;
    });
  }

  return watches;
}

function getAngularEventHandlers(symbol) {
  let handlers = ``;

  const metadata = symbol['ui5-metadata'];
  const events = metadata.events;

  if (events && events.length) {
    events.forEach(function(event) {
      handlers += `
      if (this.oui5${getPropertyCamelCase(event.name)}) {
        this.component.attach${getPropertyCamelCase(event.name)}((event: any) => {
          this.oui5${getPropertyCamelCase(event.name)}({event: event});
          this.$scope.$applyAsync();
        });
      }
      `;
    });
  }

  return handlers;
}

function getInstanceVariables(symbol) {
  let variables = ``;

  const metadata = symbol['ui5-metadata'];
  const properties = metadata.properties;

  if (properties && properties.length) {
    properties.forEach(function(property) {
      variables += `oui5${getPropertyCamelCase(property.name)}: any;\n`;
    });
  }

  return variables;
}

function getInstanceFunctions(symbol) {
  let functions = ``;

  const metadata = symbol['ui5-metadata'];
  const events = metadata.events;

  if (events && events.length) {
    events.forEach(function(event) {
      functions += `oui5${getPropertyCamelCase(event.name)}: any;\n`;
    });
  }

  return functions;
}

function getComponentInitializationCode(symbol, filesMap) {
  if (isOUI5Control(symbol)) {
    return `
        this.model = new (<any>sap).ui.model.json.JSONModel({
          ${getOUI5ModelValues(symbol, filesMap)}
        });

        this.component = new ${getInstantiateObject(symbol.name)}(this.$attrs.oui5Id, {
          ${getOUI5ComponentValues(symbol, filesMap)}
        });
        this.component.setModel(this.model);
    `;
  } else {
    return ``;
  }
}

function getComponentTemplate(symbol, filesMap) {
  return `
  ${getControllerTemplate(symbol, filesMap)}

  angular.module('oui5').component('${getComponentName(symbol.name)}', {
    controller: ${getControllerName(symbol.name)}Controller,
    transclude: true,
    template: '<ng-transclude></ng-transclude>',
    bindings: {
      oui5Id: '@',
      oui5Class: '@',
      oui5Aggregation: '@',
      oui5Association: '@',
      oui5Init: '&',
      ${getAngularBindings(symbol, filesMap)}
    }
  });
  `;
}

function getControllerTemplate(symbol, filesMap) {
  return `
  ${!!symbol.extends ? `import { ${getControllerName(symbol.extends)}Controller } from '${getFolderPackage(symbol.extends)}/${getFileName(symbol.extends)}.component';` : ``}

  export class ${getControllerName(symbol.name)}Controller ${!!symbol.extends ? `extends ${getControllerName(symbol.extends)}Controller ` : ``}{

    static $inject = ['$scope', '$element', '$attrs'];

    ${!!symbol.extends ? `` : `
      $scope: any;
      $element: any;
      $attrs: any;

      oui5Aggregation: any;
      oui5Association: any;
      oui5Init: Function;

      component: any;
      model: any;
      `}

    ${getInstanceVariables(symbol)}
    ${getInstanceFunctions(symbol)}

    constructor(
      $scope: any,
      $element: any,
      $attrs: any
    ) {
      ${!!symbol.extends ? `
        super($scope, $element, $attrs);
        ` : `
        this.$scope = $scope;
        this.$element = $element;
        this.$attrs = $attrs;
        `}
    }

    $onInit() {
        ${getComponentInitializationCode(symbol, filesMap)}

        this.onInit();
    }

    // Anything that needs to be initialized through inheritance
    onInit() {
      ${!!symbol.extends ? `super.onInit();` : `
        if (this.component.addStyleClass) {
          this.component.addStyleClass(this.$attrs.oui5Class);
        }

        if (this.component.placeAt) {
          this.component.placeAt(this.$element);
        }

        $(this.$element).hide();
        this.component.addEventDelegate({
            onAfterRendering: () => {
                // FIXME: Something broke here.
                // this.$element.children().append(this.component.$());
                // this.$element.children()[0].append(this.$element.children()[1]);
                $(this.$element).show();
            }
        });

        this.$scope.$on('aggregation', (event, data) => {
          if (this.component !== data.component) {
            const aggregation: string = data.aggregation;
            const methods: string = aggregation.substring(0, 1).toUpperCase() + aggregation.substring(1, aggregation.length);
            const method: string = methods.substring(0, methods.length - 1);
            const adds: Function = this.component['add' + methods];
            const add: Function = this.component['add' + method];
            const sets: Function = this.component['set' + methods];
            const set: Function = this.component['set' + method];

            if (add) {
              add.call(this.component, data.component);
            } else if (set) {
              set.call(this.component, data.component);
            } else if (adds) {
              adds.call(this.component, data.component);
            } else if (sets) {
              sets.call(this.component, data.component);
            } else {
              throw 'Bad aggregation';
            }

            event.stopPropagation();
          }
        });

        this.$scope.$on('association', (event, data) => {
          if (this.component !== data.component) {
            const association: string = data.association;
            const methods: string = association.substring(0, 1).toUpperCase() + association.substring(1, association.length);
            const method: string = methods.substring(0, methods.length - 1);
            const adds: Function = this.component['add' + methods];
            const add: Function = this.component['add' + method];
            const sets: Function = this.component['set' + methods];
            const set: Function = this.component['set' + method];

            if (add) {
              add.call(this.component, data.component);
            } else if (set) {
              set.call(this.component, data.component);
            } else if (adds) {
              adds.call(this.component, data.component);
            } else if (sets) {
              sets.call(this.component, data.component);
            } else {
              throw 'Bad association';
            }

            event.stopPropagation();
          }
        });

        if (this.$attrs.oui5Aggregation) {
          this.$scope.$emit('aggregation', {
            aggregation: this.$attrs.oui5Aggregation,
            component: this.component
          });
        }

        if (this.$attrs.oui5Association) {
          this.$scope.$emit('association', {
            association: this.$attrs.oui5Association,
            component: this.component
          });
        }

        if (this.oui5Init) {
          this.oui5Init({component: this.component});
        }

        this.model.attachPropertyChange((event) => {
          const path = event.getParameters().path;
  				const key = path.substring(1, path.length);
  				const value = event.getParameters().value;

  				this[key] = value;

          this.$scope.$applyAsync();
        }, this);
        `}

      ${getAngularPropertyWatches(symbol)}

      ${getAngularEventHandlers(symbol)}
    }

    $onDestroy() {
      ${isOUI5Control(symbol) ? `
      if (this.component) {
        this.component.destroy();
        this.component = null;
      }

      if (this.model) {
        this.model.detachPropertyChange();
        this.model.destroy();
        this.model = null;
      }
      ` : ``
      }
    }
  }
  `;
}

function getStubsTemplate() {
  return `
  declare var $: any;
  declare var angular: any;
  declare var sas: any;
  declare var sap: any;

  function define(arg1, arg2, arg3?): any {
    if (typeof arg1 !== 'string') {
      // remove relative
      const imports = [];
      arg1.forEach(function(name) {
        if(name.indexOf('./') === 0) {
          imports.push(name.substring(2, name.length));
        } else {
          imports.push(name);
        }
      });
      (<any>sap).ui.define(imports, arg2);
    } else {
      let name = arg1;

      if(name.indexOf('./') === 0) {
        name = name.substring(2, name.length);
      }

      (<any>sap).ui.define(name, arg2, arg3);
    }
  }
  function require(arg1, arg2?): any {
    if (typeof arg1 !== 'string') {
      // remove relative
      const imports = [];
      arg1.forEach(function(name) {
        if(name.indexOf('./') === 0) {
          imports.push(name.substring(2, name.length));
        } else {
          imports.push(name);
        }
      });
      (<any>sap).ui.require(imports, arg2);
    } else {
      let name = arg1;

      if(name.indexOf('./') === 0) {
        name = name.substring(2, name.length);
      }

      (<any>sap).ui.require(name);
    }
  }
`;
}

function getOui5ComponentTemplate() {
  return `

  export class Oui5ControlController {
    static $inject = ['$scope', '$element', '$attrs'];

    $scope: any;
    $element: any;
    $attrs: any;

    constructor($scope: any, $element: any, $attrs: any) {
      this.$scope = $scope;
      this.$element = $element;
      this.$attrs = $attrs;
    }

    $onInit() {
      var oui5Angular = new sas.angular.Control();

      this.$scope.$on('aggregation', (event, data) => {
        if (oui5Angular !== data.component) {
          oui5Angular.addContent(data.component);

          event.stopPropagation();
        }
      });

      if (this.$attrs.oui5Aggregation) {
        this.$scope.$emit('aggregation', {
          aggregation: this.$attrs.oui5Aggregation,
          component: oui5Angular
        });
      }

      if (this.$attrs.oui5Association) {
        this.$scope.$emit('association', {
          association: this.$attrs.oui5Association,
          component: oui5Angular
        });
      }

      $(this.$element).hide();
      oui5Angular.addEventDelegate({
          onAfterRendering: () => {
              oui5Angular.$().append(this.$element);
              $(this.$element).show();
          }
      });
    }
  }

  angular.module('oui5').component('oui5Control', {
    transclude: true,
    template: '<ng-transclude></ng-transclude>',
    controller: Oui5ControlController,
    bindings: {
      oui5Aggregation: '@',
      oui5Association: '@'
    }
  });
  `;
}

function getOui5ControlTemplate() {
  return `
  sap.ui.define('oui5Control', [
    "sap/ui/core/Control"
  ], function (Control) {
    "use strict";
    return Control.extend("sas.angular.Control", {
      metadata : {
        aggregations: {
          content: { type: 'sap.ui.core.Control', multiple: true }
        }
      },
      init : function () {
      },
      renderer : function (oRM, oControl) {
        oRM.write("<div");
        oRM.writeControlData(oControl);
        oRM.addClass("oui5-angular-control");
        oRM.writeClasses();
        oRM.write(">");

        var contents = oControl.getAggregation('content');
        if (contents) {
          for (var i = 0; i < contents.length; i++) {
            var content = contents[i];
            oRM.renderControl(content);
          }
        }

        oRM.write("</div>");
      }
    });
  });
  `;
}

function getDefineJSTemplate(name, filesMap, startsWith) {
  const filteredList = [];
  const files = Object.keys(filesMap);
  files.forEach(function(file) {
    if(file.indexOf(startsWith) === 0) {
      filteredList.push(file);
    }
  });
  let template = `
  define('${name}', [\n`;

  template += `'oui5Control.component',\n`;
  template += `'oui5Control',\n`;

  for (var i = 0; i < filteredList.length; i++) {
    const file = filteredList[i];
    const symbol = filesMap[file];
    const controllerName = getControllerName(file);

    const extension = isOUI5Control(symbol) ? 'component' : 'controller';

    template += `  '${getFolderPackage(file)}/${getFileName(file)}.component'${i === filteredList.length - 1 ? '\n' : ',\n'}`;
  }

  template += `], function() {});`;
  return template;
}
