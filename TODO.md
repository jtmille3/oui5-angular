# List of TODO's

Get working angular component wrappers generated from the jsdoc.

## Make it work
- [x] Generate a file for each control.  Ensure names are unique.
- [x] Generate a skeleton angular component for each control.
- [x] Generate a controller for each component.  
  - [x] Include extends.  Call super for each.
- [x] Generate requirejs file.
- [x] Compile TS
  - [x] Add tsconfig.json
- [x] Test harness
- [x] ID and class attributes
- [x] Add bindings
  - [x] property
  - [x] association
  - [x] aggregation
  - [x] event
- [x] Function to retrieve components.
- [x] Add watches for each property between oui5 and angular.  
- [x] Add event handlers for each event between oui5 and angular.  
  - [x] Make sure it's wrapped in $apply to trigger digest.
- [x] Work out transclude/require mechanism
- [x] Working Prototype!

## Make it right, make it fast

- [ ] Is there a more efficient way to catch and trigger bindings?
  - [ ] Look at $scope.$watch
  - [ ] Look at 2-way binding with '='
  - [ ] Look at individual model binding on UI5 components
- [ ] Look at removing transcludes.  Seems unnecessary.
- [ ] Look at doing aggregations and associations different
- [ ] Look into default aggregation and association.
- [ ] Build a minified version.
